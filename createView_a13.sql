USE db_grad_cs_1917;

 CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`%` 
    SQL SECURITY DEFINER
VIEW `a13_deal_view` AS
    SELECT 
        `d`.`deal_id` AS `deal_id`,
        `d`.`deal_time` AS `deal_time`,
        `d`.`deal_type` AS `deal_type`,
        `d`.`deal_amount` AS `deal_amount`,
        `d`.`deal_quantity` AS `deal_quantity`,
        `c`.`counterparty_name` AS `counterparty_name`,
        `c`.`counterparty_status` AS `counterparty_status`,
        `c`.`counterparty_date_registered` AS `counterparty_date_registered`,
        `i`.`instrument_name` AS `instrument_name`
    FROM
        ((`deal` `d`
        LEFT JOIN `counterparty` `c` ON ((`d`.`deal_counterparty_id` = `c`.`counterparty_id`)))
        LEFT JOIN `instrument` `i` ON ((`i`.`instrument_id` = `d`.`deal_instrument_id`))) 